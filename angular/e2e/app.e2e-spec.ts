import { JaiboonTemplatePage } from './app.po';

describe('Jaiboon App', function() {
  let page: JaiboonTemplatePage;

  beforeEach(() => {
    page = new JaiboonTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
