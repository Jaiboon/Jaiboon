﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Jaiboon.Skills
{
    [Table("AppSkills")]
    public class Skill : Entity, IHasCreationTime
    {
        [Required]
        public virtual string Title { get; protected set; }

        public virtual string Description { get; protected set; }

        [Required]
        public virtual int Value { get; protected set; }

        public DateTime CreationTime { get; set; }

        protected Skill()
        {
            CreationTime = Clock.Now;
        }

        public Skill(string title, string description = null, int value = 0)
            : this()
        {
            Title = title;
            Description = description;
            Value = value;
        }
    }
}
