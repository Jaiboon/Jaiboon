﻿using Abp.Authorization;
using Jaiboon.Authorization.Roles;
using Jaiboon.Authorization.Users;

namespace Jaiboon.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
