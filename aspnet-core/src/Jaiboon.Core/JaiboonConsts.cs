﻿namespace Jaiboon
{
    public class JaiboonConsts
    {
        public const string LocalizationSourceName = "Jaiboon";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
