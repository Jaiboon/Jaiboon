using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace Jaiboon.Controllers
{
    public abstract class JaiboonControllerBase: AbpController
    {
        protected JaiboonControllerBase()
        {
            LocalizationSourceName = JaiboonConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
