﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Jaiboon.Authorization.Roles;
using Jaiboon.Authorization.Users;
using Jaiboon.MultiTenancy;
using Jaiboon.Skills;

namespace Jaiboon.EntityFrameworkCore
{
    public class JaiboonDbContext : AbpZeroDbContext<Tenant, Role, User, JaiboonDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Skill> Skills { get; set; }

        public JaiboonDbContext(DbContextOptions<JaiboonDbContext> options)
            : base(options)
        {
        }
    }
}
