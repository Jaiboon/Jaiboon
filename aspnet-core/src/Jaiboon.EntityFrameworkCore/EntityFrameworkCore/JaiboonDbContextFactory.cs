﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Jaiboon.Configuration;
using Jaiboon.Web;

namespace Jaiboon.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class JaiboonDbContextFactory : IDesignTimeDbContextFactory<JaiboonDbContext>
    {
        public JaiboonDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<JaiboonDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            JaiboonDbContextConfigurer.Configure(builder, configuration.GetConnectionString(JaiboonConsts.ConnectionStringName));

            return new JaiboonDbContext(builder.Options);
        }
    }
}
