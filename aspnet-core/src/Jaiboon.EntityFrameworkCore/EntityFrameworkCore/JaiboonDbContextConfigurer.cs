using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Jaiboon.EntityFrameworkCore
{
    public static class JaiboonDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<JaiboonDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<JaiboonDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
