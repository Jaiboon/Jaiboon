﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using Jaiboon.Configuration.Dto;

namespace Jaiboon.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : JaiboonAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
