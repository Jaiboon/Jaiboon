﻿using System.Threading.Tasks;
using Jaiboon.Configuration.Dto;

namespace Jaiboon.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
