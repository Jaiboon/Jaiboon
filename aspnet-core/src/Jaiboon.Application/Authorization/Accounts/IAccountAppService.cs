﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Jaiboon.Authorization.Accounts.Dto;

namespace Jaiboon.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
