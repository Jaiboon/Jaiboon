﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Jaiboon.Authorization;

namespace Jaiboon
{
    [DependsOn(
        typeof(JaiboonCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class JaiboonApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<JaiboonAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(JaiboonApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
