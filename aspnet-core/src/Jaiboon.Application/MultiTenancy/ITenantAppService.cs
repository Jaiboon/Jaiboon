﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Jaiboon.MultiTenancy.Dto;

namespace Jaiboon.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
