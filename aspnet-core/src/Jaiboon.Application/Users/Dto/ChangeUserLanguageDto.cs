using System.ComponentModel.DataAnnotations;

namespace Jaiboon.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}