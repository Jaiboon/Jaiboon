﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Jaiboon.Sessions.Dto;

namespace Jaiboon.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
