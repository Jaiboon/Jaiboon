using Microsoft.AspNetCore.Antiforgery;
using Jaiboon.Controllers;

namespace Jaiboon.Web.Host.Controllers
{
    public class AntiForgeryController : JaiboonControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
