﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Jaiboon.Configuration;

namespace Jaiboon.Web.Host.Startup
{
    [DependsOn(
       typeof(JaiboonWebCoreModule))]
    public class JaiboonWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public JaiboonWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(JaiboonWebHostModule).GetAssembly());
        }
    }
}
